import Taro from '@tarojs/taro'
import { observable, action } from 'mobx'
import { request, formatGoodsList, formatOrderList } from '../utils'

const HOST = 'https://shangde.xianglinhu.com'

const Store = observable({
  loginStatus: false, // login status
  buyerToken: null, // user auth token

  @action.bound
  clearBuyerToken() {
    Taro.setStorageSync('buyer_token', '')
    this.buyerToken = ''
  },

  @action.bound
  async checkLogin(type) {
    const buyerToken = this.buyerToken || Taro.getStorageSync('buyer_token')

    if (type === 'buyer' && buyerToken.startsWith('buyer')) {
      this.buyerToken = buyerToken
      return true
    }
    if (type === 'admin' && buyerToken.startsWith('admin')) {
      this.buyerToken = buyerToken
      return true
    }
    // if error
    Taro.redirectTo({ url: '/pages/login/login' })
  },

  // 登陆
  async login() {
    // 获取 js_code
    const result = await Taro.login();
    if (result.errMsg !== 'login:ok') throw new Error('query js token failed')
    // 登陆
    try {
      const buyerToken = await request(
        '/api/v1/plugins/wechat/mini_app/login/query',
        'get',
        { shop_id: 1, js_code: result.code },
        true
      )
      Taro.setStorageSync('buyer_token', buyerToken)
      this.buyerToken = buyerToken
      return await this.fetchUserData(buyerToken)
    } catch (e) {
      throw new Error('login failed');
      return false;
    }
  },

  @action.bound
  async adminLogin(data) {
    const result = await request( '/api/v1/admin/login/create', 'post', data, true)
    this.buyerToken = result.token
    Taro.setStorageSync('buyer_token', result.token)
    return result
  },

  logout() {
    Taro.clearStorageSync('buyer_token')
  },

  @observable userData: { },

  async saveUserData(data) {
    return await request(`/api/v1/buyer/info/create`, 'post', data)
  },

  @action.bound
  async fetchUserData() {
    const result = await request('/api/v1/buyer/info/query', 'get')
    this.userData = result
    return result;
  },

  @action.bound
  async fetchUserDataById(id) {
    return await request('/api/v1/buyer/info/no_auth/query', 'get', { buyer_id: id, shop_id: 1 }, true)
  },

  @action.bound
  async fetchBonus() {
    const result = await request('/api/v1/bonus/query', 'get', {})
    this.userData = {
      ...this.userData,
      bonus: result.bonus
    }
    return result
  },

  // goods
  @observable goodsList: [],

  @action.bound
  async fetchGoodsList(data) {
    this.goodsList = []
    const result = await request('/api/v1/goods/query', 'get', data)
    this.goodsList = formatGoodsList(result)
  },

  @action.bound
  fetchGoods(id) {
    return this.goodsList.filter(item => item.id === id)
  },

  // order
  @observable orderList: [],

  @action.bound
  async fetchOrderList(data) {
    this.orderList = []
    const list = await request('/api/v1/order/list/query', 'get', data)
    this.orderList = formatOrderList(list)
  },

  @action.bound
  async getOrder(id) {
    return await request('/api/v1/order/query', 'get', { order_id: id })
  },

  @action.bound
  async createOrder(data) {
    return await request('/api/v1/order/create', 'post', data)
  },

  @action.bound
  async orderOperator(data) {
    return await request('/api/v1/admin/manage/order/create', 'post', data)
  },

  @action.bound
  async pay(data) {
    return await request('/api/v1/pay/bonus/create', 'post', data)
  },

  @action.bound
  async uploadFile(files) {
    const { buyerToken } = this;
    const result = await Taro.uploadFile({
      url: `${HOST}/api/v1/plugins/image/create?buyer_token=${buyerToken}&shop_id=1`,
      header: {
        'content-type': 'multipart/form-data'
      },
      filePath: files[0].url,
      name: 'image',
    })
    const data = JSON.parse(result.data || '')
    if (data.code === 0) {
      return data.data
    } else {
      throw new Error('upload file failed')
    }
  },

  // 上架
  @action.bound
  async onShelf(data) {
    return await request('/api/v1/admin/manage/goods/create', 'post', data)
  },

  @action.bound
  async createGoods(data) {
      return await request('/api/v1/goods/create', 'post', data)
  },

  @action.bound
  async updateGoods(data) {
    return await request('/api/v1/goods/update', 'post', data)
  },
})

export default Store
