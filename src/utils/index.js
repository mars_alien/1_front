import Taro from '@tarojs/taro'
import moment from 'dayjs'
import appStore from '../store/app'

export const getStatusText = (status) => {
  // 订单状态:0 已过期 1 未支付 2 已支付 4 已收货 10 已完成
  const statusMap = {
    '-1': '订单过期',
    0: '已过期',
    1: '未支付',
    2: '已支付',
    4: '已收货',
    10: '已完成',
  }

  return statusMap[status] || ''
}

const HOST = 'https://shangde.xianglinhu.com'

export async function request(url, method, data, noToken = false){
  const { buyerToken } = appStore
  let fullUrl = `${HOST}${url}`
  console.log(buyerToken)
  if (!noToken && buyerToken ) {
    if (buyerToken.search('admin') !== -1) {
      fullUrl += `?token=${buyerToken}&shop_id=1`
    } else {
      fullUrl += `?buyer_token=${buyerToken}&shop_id=1`
    }
  }
  Taro.showLoading({ title: '加载中' })
  const result = await Taro.request({
    url: fullUrl, data, method,
    header: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
  Taro.hideLoading()
  if (result && result.data && result.data.code === 0) {
    return result.data.data || {}
  }

  if (result.data.code === 1007) {
    Taro.navigateTo({ url: '/pages/login/login' })
    throw new Error('no_login')
  } else {
    throw new Error(result.data.msg)
  }
}

export function formatGoodsList (list) {
  return list.map(item => ({
    id: item.goods_id,
    name: item.goods_name,
    images: [`${HOST}${item.image_path}`],
    price: item.price,
    deposit: item.deposit,
    owner: item.buyer_id,
    ...item
  }))
}

export function formatOrderList(list) {
  return list.map(item => ({
    id: item['goods.order_id'],
    name: item['goods_name'],
    img: getImagePath(item['image_id']),
    status: item.status,
    owner: item.buyer_id
  }))
}

export function getImagePath(id) {
  return `${HOST}/statics/images/${id}`
}

export function formatData (data) {
  if (!data) {
    return ''
  }
  return moment.unix(data).format('YYYY-MM-DD HH:mm')
}
