import Taro, { Component } from '@tarojs/taro'
import { View } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import { AtTabBar } from 'taro-ui'

import Order from './order'
import Goods from './goods'
import User from './user'
import './admin.scss'

@inject('app')
@observer
class Index extends Component {

  constructor(props) {
    super(props)
    this.nav = [{
      title: '订单',
      iconType: 'home',
      key: 'market'
    }, {
      title: '商品',
      iconType: 'bullet-list',
      key: 'order'
    }, {
      title: '我的',
      iconType: 'user',
      key: 'home'
    }]

    this.state = {
      active: 0,
    }
  }

  componentDidMount() {
    const { app } = this.props
    Taro.setNavigationBarTitle({ title: '管理' })
    app.checkLogin('admin')
  }

  onChangeActive = (id) => {
    this.setState({ active: id })
  }

  render() {
    const { active } = this.state
    return (
      <View className='admin-root'>
        <View className='admin-content'>
          { active === 0 ? <Order /> : null }
          { active === 1 ? <Goods /> : null }
          { active === 2 ? <User /> : null }
        </View>
        <AtTabBar
          current={active}
          tabList={this.nav}
          onClick={this.onChangeActive}
        />
      </View>
    )
  }
}

export default Index
