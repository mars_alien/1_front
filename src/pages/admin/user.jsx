import Taro, { Component } from '@tarojs/taro'
import { View } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import { AtList, AtListItem, AtMessage } from 'taro-ui';

@inject('app')
@observer
class Index extends Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { app: {  checkLogin } } = this.props;
    Taro.setNavigationBarTitle({ title: '个人信息' })
    checkLogin('admin')
  }

  logout = async () => {
    const { app: { logout } } = this.props;
    await logout();
    Taro.redirectTo({ url: '/pages/login/login' })
  }

  render() {
    return (
      <View className='user'>
        <AtMessage />
        <AtList>
          <AtListItem title='退出登陆' arrow='right' onClick={this.logout} />
        </AtList>
      </View>
    )
  }
}

export default Index
