import Taro, { Component } from '@tarojs/taro'
import { View } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import { AtTabs, AtTabsPane } from 'taro-ui'
import GoodsList from './goods-list'

import './admin.scss'

@inject('app')
@observer
class Index extends Component {
  constructor(props) {
    super(props)
    this.info = [
      { number: 1, title: '未借出' },
      { number: 1, title: '已借出' },
      { number: 1, title: '待上架' },
    ]
    this.state = {
      active: 0
    }
  }

  componentDidMount() {
    Taro.setNavigationBarTitle({ title: '商品管理' })
  }

  componentDidShow() {
    this.onChangeActive(this.state.active)
  }

  fetchGoods = async (params) => {
    const { app: { fetchGoodsList } } = this.props;
    await fetchGoodsList(params)
  }

  onChangeActive = async (id) => {
    this.setState({ active: id })
    if (id === 0) {
      this.fetchGoods({ on_shelf: 1, stock: 1 })
    }
    if (id === 1) {
      this.fetchGoods({ on_shelf: 1, stock: 0 })
    }
    if (id === 2) {
      this.fetchGoods({ on_shelf: 0 })
    }
  }

  render() {
    const { info } = this
    const { active } = this.state;
    const { app: { goodsList } } = this.props
    return (
      <View>
        <AtTabs
          animated={false}
          current={active}
          tabList={info}
          onClick={this.onChangeActive}
        >
          <AtTabsPane current={active} index={0}>
            <GoodsList list={goodsList} isAdmin />
          </AtTabsPane>
          <AtTabsPane current={active} index={1}>
            <GoodsList list={goodsList} isAdmin />
          </AtTabsPane>
          <AtTabsPane current={active} index={2}>
            <GoodsList list={goodsList} isAdmin action='ON_SHELF' />
          </AtTabsPane>
        </AtTabs>
      </View>
    )
  }
}

export default Index
