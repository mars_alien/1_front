import Taro, { Component } from '@tarojs/taro'
import { View } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import { AtTabs, AtTabsPane } from 'taro-ui'
import OrderList from '../order/order-list'

import './admin.scss'

@inject('app')
@observer
class Index extends Component {
  constructor(props) {
    super(props)
    this.nav = [
      {
        title: '工作台',
        iconType: 'home',
        key: 'market'
      },
      {
        title: '消息',
        iconType: 'add',
        key: 'publish'
      },
      {
        title: '订单',
        iconType: 'bullet-list',
        key: 'order'
      }
    ]
    this.info = [
      { number: 1, title: '待归还' },
      { number: 2, title: '待取货' },
    ]
    this.state = {
      active: 0
    }
  }

  componentDidMount() {
    Taro.setNavigationBarTitle({ title: '订单管理' })
  }

  componentDidShow() {
    this.onChangeActive(this.state.active)
  }

  onChangeActive = id => {
    const { app } = this.props;
    this.setState({ active: id })
    if (id === 0) {
      app.fetchOrderList({ status: 4 })
    }
    if (id === 1) {
      app.fetchOrderList({ status: 2 })
    }
  }

  render() {
    const { info } = this
    const { active } = this.state;
    const { app: { orderList } } = this.props
    return (
      <View>
        <AtTabs
          animated={false}
          current={active}
          tabList={info}
          onClick={this.onChangeActive}
        >
          <AtTabsPane current={active} index={0}>
            <OrderList list={orderList} isAdmin action="return" />
          </AtTabsPane>
          <AtTabsPane current={active} index={1}>
            <OrderList list={orderList} isAdmin action="lend" />
          </AtTabsPane>
        </AtTabs>
      </View>
    )
  }
}

export default Index
