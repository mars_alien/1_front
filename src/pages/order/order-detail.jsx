import Taro, { Component } from '@tarojs/taro'
import { observer, inject } from '@tarojs/mobx'
import { Image, View } from '@tarojs/components';
import { AtList, AtListItem, AtButton, AtTag, AtCard, AtMessage } from 'taro-ui';

import './order.scss'
import { getStatusText, getImagePath, formatData } from '../../utils';

@inject('app')
@observer
class Index extends Component {

  constructor(props) {
    super(props);
    this.state = {
      id: '',
      isAdmin: false,
      data: {},
      owner: {},
      buyer: {},
      action: '',
    }
  }

  componentDidMount() {
    Taro.setNavigationBarTitle({ title: '订单详情' })
  }

  componentDidShow() {
    this.getData()
  }

  getData = async () => {
    const { app: { getOrder, fetchUserDataById } } = this.props
    const { isAdmin, id, action } = this.$router.params
    const order = await getOrder(id)
    const owner = await fetchUserDataById(order.owner_id)
    const buyer = await fetchUserDataById(order.buyer_id)

    this.setState({
      isAdmin: isAdmin === 'true',
      data: order,
      owner,
      buyer,
      action,
      id
    })
  }

  onTake = () => {
    const { app } = this.props
    const { id } = this.state;
    app.orderOperator({ order_id: id, operation: 'lend' })
    Taro.atMessage({ message: '操作成功', type: 'success' })
    setTimeout(() => { Taro.navigateBack() }, 1000)
  }

  onReturn = () => {
    const { app } = this.props
    const { id } = this.state;
    app.orderOperator({ order_id: id, operation: 'return' })
    Taro.atMessage({ message: '操作成功', type: 'success' })
    setTimeout(() => { Taro.navigateBack() }, 1000)
  }

  renderAction = () => {
    const { action, isAdmin } = this.state
    if (isAdmin && action === 'lend') {
      return <AtButton type='primary' onClick={this.onTake} className='app-bottom-btn'>标记为已取货</AtButton>
    }
    if (isAdmin && action === 'return') {
      return <AtButton type='primary' onClick={this.onReturn} className='app-bottom-btn'>标记为已归还</AtButton>
    }
  }

  render() {
    const { isAdmin, data, owner, buyer } = this.state

    return (
      <View className='order-detail'>
        <AtMessage />
        <View className='app-content'>
          <View className='order-detail__goods'>
            <Image src={getImagePath(data.image_id)} className='order-detail__goods-image' />
            <View className='order-detail__goods-info'>
              <View className='order-detail__goods-name'>{data.goods_name}</View>
              <View className='order-detail__goods-status'>
                <AtTag>{getStatusText(data.status)}</AtTag>
              </View>
            </View>
          </View>
            <View className='order-detail__content'>
              <AtCard title='订单详情'>
                  <AtList>
                    <AtListItem title='开始时间' extraText={formatData(data.create_time)} />
                    <AtListItem title='租借押金' extraText={`${data.deposit || ''} 积分`} />
                    <AtListItem title='租借单价' extraText={`${data.price || ''} 积分/天`} />
                  </AtList>
              </AtCard>
            </View>
          {
            isAdmin && (
              <View className='order-detail__content'>
                <AtCard title='买家信息'>
                  <AtList>
                    <AtListItem title='租借者' extraText={buyer.name || ''} key='buyer-name' />
                    <AtListItem title='租借者班级' extraText={buyer.class || ''} key='buyer-name' className='no-bottom' />
                  </AtList>
                </AtCard>
              </View>
            )
          }
          {
            isAdmin && (
              <View className='order-detail__content'>
                <AtCard title='分享者信息'>
                  <AtList>
                    <AtListItem title='分享者' extraText={owner.name || ''} />
                    <AtListItem title='分享者班级' extraText={owner.class || ''}  className='no-bottom' />
                  </AtList>
                </AtCard>
              </View>
            )
          }
        </View>
        {this.renderAction()}
      </View>
    )
  }
}

export default Index
