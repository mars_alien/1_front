import Taro, { Component } from '@tarojs/taro'
import { View, Image } from '@tarojs/components'
import { AtTag } from 'taro-ui';

import './order.scss'
import { getStatusText } from '../../utils'

class Index extends Component {
  constructor(props) {
    super(props)
  }

  onClick = (id) => {
    const { isAdmin, action } = this.props;
    Taro.navigateTo({ url: `/pages/order/order-detail?isAdmin=${isAdmin}&id=${id}&action=${action}`, })
  }

  render() {
    const list = this.props.list || [];
    return (
      <View className='order-detail'>
        {
          list.map(item => {
            return (
              <View key={`order-item-${item.id}`} className='order-list__item' onClick={() => this.onClick(item.id)}>
                <Image src={item.img} className='order-item__img' />
                <View className='order-item__content'>
                  <View className='order-item__name' >{item.name || ''}</View>
                  <View className='order-item__status'>
                    <AtTag>{getStatusText(item.status)}</AtTag>
                  </View>
                </View>
              </View>
            )
          })
        }
      </View>
    )
  }
}

export default Index
