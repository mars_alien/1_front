import Taro, { Component } from '@tarojs/taro'
import { View } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import { AtForm, AtInputNumber, AtCard, AtList, AtListItem, AtInput, AtButton, AtMessage } from 'taro-ui';

import './order.scss'

@inject('app')
@observer
class Order extends Component {

  constructor(props) {
    super(props)
    this.state = {
      data: {},
      goodsId: '',
      owner: {}
    }
  }

  componentDidMount () {
    Taro.setNavigationBarTitle({ title: '订单确认' })
    this.getDate()
  }

  getDate = async () => {
    const { app: { fetchGoods, fetchUserDataById } } = this.props;
    const { id } = this.$router.params;
    let goods = fetchGoods(id);
    if (goods.length === 0) {
      return false;
    }
    let owner = await fetchUserDataById(goods[0].owner)
    this.setState({
      owner,
      data: goods[0],
      goodsId: id,
    })
  }

  onCancel = () => {
    Taro.navigateBack();
  }

  onConfirm = async () => {
    const { data, goodsId } = this.state;
    const { app } = this.props;
    const { createOrder } = app;
    console.log('hello', this.props)
    try {
      const order = await createOrder({
        goods_id: goodsId,
        price: data.price,
        deposit: data.deposit,
        counts: 1,
        charge_type: 1,
      })
      console.log(order)
      app.pay({ order_id: order.order_id })
      Taro.atMessage({ message: '创建订单成功，请前往管理处取走租借物品', type: 'success' })
      setTimeout(() => {
        Taro.redirectTo({ url: '/pages/index/index' })
      }, 1000)
    } catch (e) {
      Taro.atMessage({ message: e.message, type: 'error' })
    }
  }

  render() {
    const { data, owner } = this.state;
    const { app: { userData }} = this.props
    return (
      <View className='order-confirm'>
        <AtMessage />
        <View className='order-confirm__detail'>
          <AtCard title='确认租借物信息'>
            <AtList>
              <AtListItem title='物品' extraText={data.name || ''} />
              <AtListItem title='分享者' extraText={owner.name || ''} />
              <AtListItem title='分享者班级' extraText={owner.class || ''} />
              <AtListItem title='租借押金' extraText={`${data.deposit || ''} 积分`} />
              <AtListItem title='租借价格' extraText={`${data.price || ''} 积分/天`} />
            </AtList>
          </AtCard>
          <AtCard title='确认租借人信息'>
            <AtForm>
              <AtList>
                <AtListItem title='姓名' extraText={userData.name || ''} />
                <AtListItem title='手机号' extraText={userData.mobile || ''} />
              </AtList>
            </AtForm>
          </AtCard>
        </View>
        <View className='app-bottom-btn-group'>
          <AtButton type='secondary' onClick={this.onCancel} className='app-bottom-btn'>取消</AtButton>
          <AtButton type='primary' onClick={this.onConfirm} className='app-bottom-btn'>租借</AtButton>
        </View>
      </View>
    )
  }
}

export default Order
