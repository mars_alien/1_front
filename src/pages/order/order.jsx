import Taro, { Component } from '@tarojs/taro'
import { View } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import OrderList from './order-list'

import './order.scss'

@inject('app')
@observer
class Order extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    const { app } = this.props
    app.checkLogin('buyer')
    Taro.setNavigationBarTitle({ title: '我的订单' })
  }

  componentDidShow() {
    this.getData()
  }

  getData = async (params) => {
    const {app} = this.props
    app.fetchOrderList(params)
  }


  render() {
    const { app: { orderList } } = this.props
    return (
      <View className='order-list'>
          <OrderList list={orderList} />
      </View>
    )
  }
}

export default Order
