import Taro, { Component } from '@tarojs/taro'
import { View } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import { AtTabBar } from 'taro-ui';

import Upload from '../upload/upload';
import User from '../user';
import Market from '../market/market';
import './index.scss'

@inject('app')
@observer
class Index extends Component {

  constructor(props) {
    super(props);
    this.nav = [{
      title: '市场',
      iconType: 'home',
      key: 'market'
    }, {
      title: '分享',
      iconType: 'add',
      key: 'publish'
    }, {
      title: '我的',
      iconType: 'user',
      key: 'home'
    }];
    this.state = {
      active: 0,
      loadBuyerTokenFlag: false
    }
  }

  componentDidMount() {
    const { app: { checkLogin } } = this.props;
    checkLogin('buyer');
    setTimeout(() => {
      this.setState({ loadBuyerTokenFlag: true })
    }, 1000)
  }

  config = {
    navigationBarTitleText: '首页'
  }

  onChangeActive = (id) => {
    this.setState({ active: id })
  }

  render() {
    const { active, loadBuyerTokenFlag } = this.state;
    if (!loadBuyerTokenFlag) {
      return <View></View>
    }
    return (
      <View className='index-root'>
        <View className='index-content'>
          { active === 0 ? <Market /> : null }
          { active === 1 ? <Upload /> : null }
          { active === 2 ? <User /> : null }
        </View>
        <AtTabBar
          current={active}
          tabList={this.nav}
          onClick={this.onChangeActive}
        />
      </View>
    )
  }
}

export default Index
