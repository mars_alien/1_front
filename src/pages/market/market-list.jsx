import Taro, { Component } from '@tarojs/taro'
import { View, Image } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import { AtSearchBar, AtMessage } from 'taro-ui';

import './market.scss'

@inject('app')
@observer
class Index extends Component {

  constructor(props) {
    super(props);
    this.state = {
      searchValue: ''
    }
  }

  componentDidMount() {
    Taro.setNavigationBarTitle({ title: '货架' })
  }

  componentDidShow() {
    this.getData()
  }

  getData = async (params = {}) => {
    const { app: { fetchGoodsList } } = this.props;
    const { type } = this.$router.params;
    try {
      await fetchGoodsList({ on_shelf: 1, stock: 1, category_id: type, ...params })
    } catch (e) {
      Taro.atMessage({ message: '获取货架失败', type: 'error' })
    }
  }

  onSearchValueChange = (value) => {
    this.setState({ searchValue: value })
  }

  onSearch = async () => {
    const { searchValue } = this.state
    await this.getData({ goods_name: searchValue })
  }

  onSelect = (id) => {
    Taro.navigateTo({ url: `/pages/market/market-detail?id=${id}&action=BORROW` })
  }

  render() {
    const { searchValue } = this.state;
    const { app: { goodsList } } = this.props;

    if (!goodsList) {
      return null;
    }

    return (
      <View className='market-list'>
        <AtMessage />
        <AtSearchBar
          actionName='搜索'
          value={searchValue}
          onChange={this.onSearchValueChange}
          onActionClick={this.onSearch}
        />
        <View className='market-list__list'>
        {
          goodsList.map(item => (
            <View className='market-list__item' key={`goods-list${item.id}`} onClick={() => this.onSelect(item.id)}>
              <View className='market-list__content'>
                <Image src={item.images[0]} mode='aspectFill' className='market-list__content-bg' />
                <View className='market-list__content-name'>{item.name || ''}</View>
              </View>
            </View>
          ))
        }
        </View>
      </View>
    )
  }
}

export default Index
