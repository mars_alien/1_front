import Taro, { Component } from '@tarojs/taro'
import { observer, inject } from '@tarojs/mobx'
import { Swiper, SwiperItem, View, Image } from '@tarojs/components';
import { AtList, AtListItem, AtButton, AtMessage } from 'taro-ui';

import './market.scss'

@inject('app')
@observer
class Index extends Component {

  constructor(props) {
    super(props);
    this.bottom = [
      {
        title: '确定租借'
      }
    ]
    this.state = {
      isAdmin: false,
      action: '',
      data: { images: [] },
      owner: {},
    }
  }

  componentDidMount() {
    Taro.setNavigationBarTitle({ title: '商品详情' })
    this.getDate()
  }

  getDate = async () => {
    const { app: { fetchGoods, fetchUserDataById } } = this.props;
    const { id, isAdmin, action } = this.$router.params;
    let goods = fetchGoods(id);
    if (goods.length === 0) {
      return false;
    }
    let owner = await fetchUserDataById(goods[0].owner)
    this.setState({
      data: goods[0],
      isAdmin: isAdmin === 'true',
      action: action,
      owner: owner
    })
  }

  onConfirm = (id) => {
    Taro.navigateTo({ url: `/pages/order/order-confirm?id=${id}` })
  }

  onShelf = async (goodsId) => {
    console.log(goodsId)
    const { app } = this.props;
    try {
      await app.onShelf({ goods_id: goodsId, on_shelf: 1 })
      Taro.atMessage({ message: '上架成功', type: 'success' })
      setTimeout(() => { Taro.navigateBack() }, 1000)
    } catch (e) {
      Taro.atMessage({ message: '上架失败', type: 'error' })
    }
  }

  onEdit = (id) => {
    Taro.navigateTo({ url: `/pages/upload/edit?id=${id}` })
  }

  render() {
    const { isAdmin, data, action, owner } = this.state;

    if (!data) {
      return false;
    }

    return (
      <View className='market-detail'>
        <AtMessage />
        <Swiper
          className='market-detail-cover'
          ide indicatorColor='#999'
          indicatorActiveColor='#333'
          circular
          indicatorDots
          autoplay
        >
          {
            data.images.map((item, index) => (
              <SwiperItem className='market-detail-cover__item' key={`market-detail-img-${index}`}>
                <Image className='market-detail-cover__img' src={item} mode='aspectFill' />
              </SwiperItem>
            ))
          }
        </Swiper>
        <View className='market-detail__content'>
          <AtList>
            <AtListItem title='物品' extraText={data.name || ''} />
            <AtListItem title='价钱' extraText={`${data.price || ''} 积分/天`} />
            <AtListItem title='押金' extraText={`${data.deposit || ''} 积分`} />
            <AtListItem title='分享者' extraText={owner['name'] || ''} />
            <AtListItem title='分享者班级' extraText={owner['class'] || ''} />
          </AtList>
        </View>
        {
          !isAdmin && action === 'BORROW' && <AtButton type='primary' onClick={() => this.onConfirm(data.id)} className='app-bottom-btn'>确定租借</AtButton>
        }
        {
          action === 'ON_SHELF' && <AtButton type='primary' onClick={() => this.onShelf(data.id)} className='app-bottom-btn'>确认上架</AtButton>
        }
        {
          action === 'EDIT' && <AtButton type='primary' onClick={() => this.onEdit(data.id)} className='app-bottom-btn'>编辑</AtButton>
        }
      </View>
    )
  }
}

export default Index
