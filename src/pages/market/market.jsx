import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'

import './market.scss'

const BOOKS = 0;
const OTHER = 1;

@inject('app')
@observer
class Index extends Component {

  constructor(props) {
    super(props);
    this.catalog = [
      {
        text: '书籍',
        background: 'https://cdn.pixabay.com/photo/2016/03/27/19/32/blur-1283865_1280.jpg',
        id: BOOKS
      },
      {
        text: '其他',
        background: 'https://cdn.pixabay.com/photo/2019/12/28/20/31/conifer-4725680_1280.jpg',
        id: OTHER,
      }
    ];
  }

  componentDidMount() {
    const { app } = this.props
    app.checkLogin('buyer')
    Taro.setNavigationBarTitle({ title: '市场' })
  }

  onSelectCatalog = (id) => {
    Taro.navigateTo({ url: `/pages/market/market-list?type=${id}` })
  }

  render() {
    const { catalog } = this;

    return (
      <View className='market-cover'>
        <View className='market-cover__title'><Text>选择你想要租借的物品</Text></View>
        {
          catalog.map(item => (
            <View key={item.id} className='market-cover__item' onClick={() => this.onSelectCatalog(item.id)}>
              <View className='market-cover__content'>
                <Image src={item.background} className='market-cover__content-bg' mode='aspectFill' />
                <Text className='market-cover__content-name'>{item.text || ''}</Text>
              </View>
            </View>
          ))
        }
      </View>
    )
  }
}

export default Index
