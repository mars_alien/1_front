import Taro, { Component } from '@tarojs/taro'
import { View, Image } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import { AtTag } from "taro-ui";
import './market.scss'
import {getImagePath} from "../../utils";

@inject('app')
@observer
class Share extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    const { app } = this.props
    app.checkLogin('buyer')
    Taro.setNavigationBarTitle({ title: '我的分享' })
  }

  componentDidShow() {
    this.getData()
  }

  getData = async () => {
    const { app } = this.props
    const { userData } = app
    app.fetchGoodsList({ buyer_id: userData.buyer_id })
  }

  getTag = item => {
    const result = []
    if (item.stock == 0 && item.on_shelf == 1) {
      result.push('出借中')
    }
    if (item.on_shelf == 0) {
      result.push('待上架')
    }
    if (item.on_shelf == 1 && item.stock == 1) {
      result.push('已上架')
    }
    return result
  }

  onClick = (id) => {
    Taro.navigateTo({ url: `/pages/market/market-detail?id=${id}&action=EDIT` })
  }

  render() {
    const { app: { goodsList } } = this.props
    return (
      <View className='share-list'>
        {
          goodsList.map(item => {
            return (
              <View key={`share-item-${item.id}`} className='share-list__item' onClick={() => this.onClick(item.id)}>
                <Image src={getImagePath(item.image_id)} className='share-item__img' />
                <View className='share-item__content'>
                  <View className='share-item__name' >{item.name || ''}</View>
                  <View className='share-item__status'>
                    {
                      this.getTag(item).map(str => (<AtTag>{str}</AtTag>))
                    }
                  </View>
                </View>
              </View>
            )
          })
        }
      </View>
    )
  }
}

export default Share
