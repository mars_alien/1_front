import Taro, { Component } from '@tarojs/taro'
import { View, Picker } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import { AtInput, AtImagePicker, AtButton, AtForm, AtMessage } from 'taro-ui';

import './upload.scss'

@inject('app')
@observer
class Upload extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      price: '',
      deposit: '',
      stock: 1,
      imageId: '',
      files: [],
      selector: ['书籍', '其他'],
      type: '0',
    }
  }

  componentDidShow() {
    const { app } = this.props
    app.checkLogin('buyer')
    Taro.setNavigationBarTitle({ title: '发布' })
  }

  onFileChange = async (files, type) => {
    if (type === 'remove') {
      this.setState({ files: [], imageId: '' })
      return false
    }
    const { app: { uploadFile } } = this.props
    try {
      const imageId = await uploadFile(files);
      this.setState({ imageId, files })
    } catch(e) {
      Taro.atMessage({ message: '上传文件失败', type: 'error' });
    }
  }

  handleInput = name => (value) => {
    this.setState({ [name]: value })
  }

  handleConfirm = async () => {
    const { app: { createGoods } } = this.props;
    const { name, price, deposit, stock, imageId, type } = this.state;
    if (!(name && price && deposit && imageId)) {
      Taro.atMessage({ message: '请填写完整信息', type: 'error' })
      return false;
    }
    try {
      createGoods({
        stock,
        price: Number(price),
        deposit: Number(deposit),
        goods_name: name,
        category_id: type,
        image_id: imageId,
        charge_type: 1,
      })
      Taro.atMessage({ message: '创建商品成功', type: 'success' })
      setTimeout(() => {
        Taro.navigateTo({ url: '/pages/index/index' })
      }, 2000)
    } catch (e) {
      Taro.atMessage({ message: '创建商品失败', type: 'error' })
    }
  }

  handleTypeChange = (value) => {
    this.setState({ type: value.currentTarget.value})
  }

  render() {
    const { name, price, deposit, files, selector, type } = this.state;
    return (
      <View className='share'>
        <AtMessage />
        <AtForm className='share__form'>
          <AtInput title='名称' placeholder='请输入名称' value={name} onChange={this.handleInput('name')} />
          <AtInput title='押金' type='number' placeholder='请输入押金' value={deposit} onChange={this.handleInput('deposit')} />
          <AtInput title='价钱' type='number' placeholder='请输入价钱' value={price} onChange={this.handleInput('price')}><View style={{width: '80px' }}>积分/每天</View></AtInput>
          <View className='share-list-item'>
            <View className='share-list-item__lable'>类型</View>
            <Picker className='share-list-item__content' mode='selector' range={selector} onChange={this.handleTypeChange}>
              <View className='share-list-item__content-text'>{selector[type]}</View>
            </Picker>
          </View>
        </AtForm>
        <View className='share__img' >
          <AtImagePicker className='share__img-content' title='封面' files={files} showAddBtn={files.length === 0} onChange={this.onFileChange} />
        </View>
        <View className='share__btn'>
          <AtButton type='primary' onClick={this.handleConfirm} >分享</AtButton>
        </View>
      </View>
    )
  }
}

export default Upload
