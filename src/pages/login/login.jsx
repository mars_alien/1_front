import Taro from '@tarojs/taro'
import { isEmpty } from 'lodash'
import { View, Picker } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import { AtButton, AtMessage, AtForm, AtInput } from 'taro-ui'
import './login.scss'

const IDENTIFY_ADMIN = 'admin'
const IDENTIFY_USER = 'user'

@inject('app')
@observer
class Login extends Taro.Component {
  constructor(props) {
    super(props)
    this.state = {
      identify: IDENTIFY_USER,
      loginStatus: false,
      name: '',
      password: ''
    }
    this.identify = [
      { text: '学生模式', id: IDENTIFY_USER },
      { text: '管理员模式', id: IDENTIFY_ADMIN }
    ]
  }

  componentDidMount() {
    const { app } = this.props
    Taro.setNavigationBarTitle({ title: '登陆' })
    app.clearBuyerToken()
  }

  componentDidShow() {
    // reset state
    this.setState({ loginStatus: false, identify: IDENTIFY_USER, name: '', password: '' })
  }

  onLogin = () => {
    const { identify } = this.state
    const { app } = this.props

    this.setState(
      {
        loginStatus: true
      },
      async () => {
        try {
          const result = await app.login({ identify })
          if (isEmpty(result)) {
            Taro.redirectTo({ url: '/pages/identify/identify' })
            return true
          }
          Taro.redirectTo({ url: '/pages/index/index' })
        } catch (e) {
          this.setState({ loginStatus: false })
          Taro.atMessage({ message: '登陆失败，请联系管理员', type: 'error' })
        }
      }
    )
  }

  onIdentifySelect = e => {
    const selected = this.identify.filter(
      (_, index) => index == e.detail.value
    )
    this.setState({
      identify: selected.length ? selected[0].id : IDENTIFY_USER
    })
  }

  getSelectedIdentify = () => {
    const selected = this.identify.filter(
      item => item.id === this.state.identify
    )
    return selected.length ? selected[0].text : ''
  }

  handleInput = name => value => {
    this.setState({ [name]: value })
  }

  onAdminLogin = async () => {
    const { app: { adminLogin } } = this.props
    const { name, password } = this.state
    if (!(name && password)) {
      Taro.atMessage({ message: '请输入信息', type: 'error' })
    }
    try {
      await adminLogin({ username: name, password, shop_id: 1 })
      Taro.redirectTo({ url: '/pages/admin/admin' })
    } catch(e) {
        Taro.atMessage({ message: '登陆失败，请联系管理员', type: 'error' })
    }
  }

  render() {
    const { identify } = this
    const { loginStatus, name, password } = this.state
    return (
      <View className='app-page login-root'>
        <AtMessage />
        <View className='login-form'>
          <View className={identify === IDENTIFY_USER ? 'show' : 'hidden'}>
            <AtButton
              className='app-margin-top-6'
              type='primary'
              onClick={this.onLogin}
              loading={loginStatus}
            >
              微信登录
            </AtButton>
          </View>
          <View className={identify === IDENTIFY_ADMIN ? 'show' : 'hidden'}>
            <AtForm >
              <AtInput
                title='账号'
                value={name}
                placeholder='请输入账户'
                onChange={this.handleInput('name')}
              />
              <AtInput
                title='密码'
                value={password}
                placeholder='请输入密码'
                type='password'
                onChange={this.handleInput('password')}
              />
            </AtForm>
            <AtButton
              className='app-margin-top-6'
              type='primary'
              onClick={this.onAdminLogin}
              loading={loginStatus}
            >
              登录
            </AtButton>
          </View>
          <View className='login-picker'>
            <View className='at-icon at-icon-chevron-down'></View>
            <Picker
              mode='selector'
              range={identify.map(item => item.text)}
              onChange={this.onIdentifySelect}
              className='login-picker-content'
            >
              <View>{this.getSelectedIdentify()}</View>
            </Picker>
          </View>
        </View>
      </View>
    )
  }
}

export default Login
