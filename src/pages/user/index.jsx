import Taro, { Component } from '@tarojs/taro'
import { View } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import { AtAvatar, AtList, AtListItem, AtMessage } from 'taro-ui'

import './user.scss'

@inject('app')
@observer
class Index extends Component {

  constructor(props) {
    super(props)
  }

  async componentDidMount() {
    const { app: {  fetchBonus } } = this.props
    Taro.setNavigationBarTitle({ title: '个人信息' })
    try {
      await fetchBonus()
    } catch(e) {
      Taro.atMessage({ message: '发生错误，请联系管理员', type: 'error' })
    }
  }

  componentDidShow() {
    const { app } = this.props
    app.checkLogin('buyer')
  }

  toOrder = () => {
    Taro.navigateTo({ url: '/pages/order/order' })
  }

  toShare = () => {
    Taro.navigateTo({ url: '/pages/market/share-list' })
  }

  toIdentify = () => {
    Taro.navigateTo({ url: '/pages/identify/identify' })
  }

  logout = async () => {
    const { app: { logout } } = this.props
    await logout()
    Taro.navigateTo({ url: '/pages/login/login' })
  }


  render() {
    const { app: { userData } } = this.props
    return (
      <View className='user'>
        <AtMessage />
        <View className='user__header'>
          <AtAvatar text={userData.name} circle></AtAvatar>
          <View className='user__content'>
            <View>{userData.name}</View>
            <View>积分: {userData.bonus}</View>
          </View>
        </View>
        <AtList>
          <AtListItem title='我的订单' arrow='right' onClick={this.toOrder} />
          <AtListItem title='我的分享' arrow='right' onClick={this.toShare} />
          <AtListItem title='修改信息' arrow='right' onClick={this.toIdentify} />
          <AtListItem title='退出登陆' arrow='right' onClick={this.logout} />
        </AtList>
      </View>
    )
  }
}

export default Index
