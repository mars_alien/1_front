import Taro, { Component } from '@tarojs/taro';
import { View } from '@tarojs/components';
import { observer, inject } from '@tarojs/mobx';
import { AtForm, AtInput, AtButton, AtCheckbox, AtMessage } from 'taro-ui';

import './identify.scss';

@inject('app')
@observer
class Index extends Component {
  constructor(props) {
    super(props);
    this.confirm = [
      {
        value: 'readed',
        label: '我已接受租借条款'
      }
    ];
    this.state = {
      agreeList: [],
      name: '',
      classes: '',
      studentNo: '',
      idCard: '',
      mobile: '',
      confirmBtnLoading: false
    };
  }

  componentDidMount() {
    const { app } = this.props
    Taro.setNavigationBarTitle({ title: '身份信息' })
    const { checkLogin } = app;
    checkLogin('buyer')
    this.getData()
  }

  getData = async () => {
    const { app } = this.props
    const userData = await app.fetchUserData()
    console.log(userData)
    if (userData.name) {
      this.setState({
        name: userData.name,
        classes: userData.class,
        studentNo: userData.student_no,
        idCard: userData.id_card,
        mobile: userData.mobile
      })
    }
  }

  onChangeInput = name => value => {
    this.setState({ [name]: value });
  };

  onReadedChange = value => {
    this.setState({ agreeList: value });
  };

  onConfirm = async () => {
    const { name, classes, studentNo, mobile, idCard, agreeList } = this.state;
    const { app } = this.props;
    if (!agreeList.length) {
      Taro.atMessage({ message: '请同意租借条款', type: 'warning' });
      return false;
    }

    if (!(name && classes && studentNo && idCard)) {
      Taro.atMessage({ message: '请填写完整信息', type: 'warning' });
      return false;
    }

    this.setState({ confirmBtnLoading: true }, async () => {
      // 保存用户信息
      try {
        await app.saveUserData({ name, class: classes, student_no: studentNo, mobile: mobile, id_card: idCard });
        Taro.atMessage({ message: '保存用户信息成功', type: 'success' });
        setTimeout(() => {
          Taro.redirectTo({ url: '/pages/index/index' });
        }, 1000)
      } catch (e) {
        Taro.atMessage({ message: '保存用户信息失败，请联系管理员', type: 'error' });
      }
      this.setState({ confirmBtnLoading: false });
    });
  };

  render() {
    const {
      name,
      classes,
      studentNo,
      mobile,
      idCard,
      agreeList,
      confirmBtnLoading
    } = this.state;
    return (
      <View className='identify-root'>
        <AtMessage />
        <AtForm className='app-form'>
          <AtInput
            name='name'
            title='姓名'
            value={name}
            placeholder='请输入姓名'
            onChange={this.onChangeInput('name')}
          />
          <AtInput
            name='classes'
            title='班级'
            value={classes}
            placeholder='请输入班级'
            onChange={this.onChangeInput('classes')}
          />
          <AtInput
            name='studentNo'
            title='学号'
            value={studentNo}
            placeholder='请输入学号'
            onChange={this.onChangeInput('studentNo')}
          />
          <AtInput
            name='mobile'
            title='手机号'
            type='phone'
            value={mobile}
            placeholder='请输入手机号'
            onChange={this.onChangeInput('mobile')}
          />
          <AtInput
            name='idCard'
            title='身份证'
            type='idcard'
            value={idCard}
            placeholder='请输入身份证'
            onChange={this.onChangeInput('idCard')}
          />
        </AtForm>
        <AtCheckbox
          options={this.confirm}
          selectedList={agreeList}
          onChange={this.onReadedChange}
          className='identify-agree app-margin-top-4'
        />
        <AtButton
          type='primary'
          className='identify-confirm-btn'
          onClick={this.onConfirm}
          loading={confirmBtnLoading}
        >
          确认
        </AtButton>
      </View>
    );
  }
}

export default Index;
